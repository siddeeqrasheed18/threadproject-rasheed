package threadProj;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * This main class will facilitate the running of the program.
 * It will prompt the user for their desired amount on producer and consumer threads, and create them.
 * It will then run those threads 100 times.
 * @author Siddeeq Rasheed
 * 
 * EXTRA CREDIT DOCUMENTATION:
 * Race condition: 
 * 	A race condition occurs when multiple threads try to access and modify the same data at the same time.
 * 	These are undesirable and can lead to unintended behavior.
 * Critical sections of code:
 * 	A critical section of code can only be accessed by one process at a time. 
 * 	This is to prevent multiple processes from modifying a shared resource at unpredictable times.
 * Locks:
 * 	A lock is a point in a program where only one process can pass through at a time. 
 * 	This can be easily done using test-and-set instruction.
 * Deadlocks:
 * 	A deadlock occurs when two or more processes that share the same resource lock each other out of accessing the shared resource.
 * 	This can also occur when multiple processes have access to a critical section of code and block each other indefinitely.
 * Conditional Locks:
 * 	A conditional lock can only be obtained/passed through by a process that satisfies an arbitrary condition, such as a variable being set to a specific value.
 */
public class Main {

	public static void main(String[] args) throws InterruptedException {
		QueueClass q = new QueueClass();
		ArrayList<Thread> producerThreads = new ArrayList<Thread>();
		ArrayList<Thread> consumerThreads = new ArrayList<Thread>();
		Producer p = new Producer(q);
		Consumer c = new Consumer(q);
		int numProds,numCons;
		
		Scanner in = new Scanner(System.in);
		System.out.println("Please enter number of producers:");
		numProds = in.nextInt();
		System.out.println("Please enter number of consumers:");
		numCons = in.nextInt();
		in.close();
		
		System.out.println("Now running with "+numProds+" producers and "+numCons+" consumers:");
		for (int i = 0; i<numProds; i++) {
			producerThreads.add(new Thread(p));
		}
		for (int i = 0; i<numCons; i++) {
			consumerThreads.add(new Thread(c));
		}
		
		int runs = 0;
		while (runs<100) {
			for (int i = 0; i<numProds; i++) {
				producerThreads.get(i).run();
			}
			for (int i = 0; i<numCons; i++) {
				consumerThreads.get(i).run();
			}
			runs++;
		}
	}

}
