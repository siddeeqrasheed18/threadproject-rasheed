package threadProj;

import java.util.Date;

/**
 * this class represents a producer. It will supply the QueueClass with Strings until it contains 10 Strings.
 * @author Siddeeq Rasheed
 *
 */
public class Producer implements Runnable {

	private QueueClass q;
	
	public Producer(QueueClass q) {
		this.q = q;
	}
	
	@Override
	public void run() {
		synchronized(q) {
			while (q.getSize()<10) {
				q.add(new Date().toString());
				System.out.println("Producer: Queue now has "+q.getSize()+" items.");
			}
		}
	}

}
