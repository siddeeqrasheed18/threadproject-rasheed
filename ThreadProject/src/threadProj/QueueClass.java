package threadProj;

import java.util.LinkedList;
import java.util.Queue;

/**
 * This class represents a Queue with synchronized add and remove methods.
 * @author Siddeeq
 *
 */
public class QueueClass {
	
	private Queue<String> q;
	
	public QueueClass() {
		this.q = new LinkedList<String>();
	}
	
	public synchronized void add(String s) {
		q.add(s);
	}
	
	public synchronized String remove() {
		return q.remove();
	}
	
	public int getSize() {
		return q.size();
	}
}
