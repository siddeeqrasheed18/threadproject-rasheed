package threadProj;

/**
 * This class represents a consumer. It will remove and print out all the Strings in the QueueClass until it is empty.
 * @author Siddeeq Rasheed
 *
 */
public class Consumer implements Runnable {

	private QueueClass q;
	
	public Consumer(QueueClass q) {
		this.q = q;
	}
	
	@Override
	public void run() {
		synchronized(q) {
			while (q.getSize()>0) {
				System.out.println("Consumer: "+q.remove());
			}
		}
	}

}
